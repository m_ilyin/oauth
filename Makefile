REBAR=`which rebar`
DIALYZER=dialyzer

all: get-deps compile

get-deps:
	@$(REBAR) get-deps

compile:
	@$(REBAR) compile

clean:
	@$(REBAR) clean

eunit:
	@$(REBAR) skip_deps=true eunit

.PHONY: test

ct:
	@$(REBAR) skip_deps=true ct

test: eunit ct

rel: deps compile
	@$(REBAR) generate

relclean:
	@rm -rf rel/coyote_interface

build-plt:
	@$(DIALYZER) --build_plt --output_plt .esmsd_dialyzer.plt \
		--apps kernel stdlib sasl #misultin emysql

dialyze:
	@$(DIALYZER) --src src --plt .esmsd_dialyzer.plt -Werror_handling \
		-Wrace_conditions -Wunmatched_returns # -Wunderspecs

docs:
	@$(REBAR) skip_deps=true doc
